import { Locator, structUtils, tgzUtils, semverUtils } from "@yarnpkg/core";
import { PortablePath, NodeFS, ppath, ZipFS } from "@yarnpkg/fslib";
import { spawnSync } from "child_process"

let fs = new NodeFS()

let NODE_PATHS = initNodePath()

function initNodePath() {
  let nPaths = [
    `/usr/share/nodejs/`,
    `/usr/lib/nodejs/`
  ]
  let DEB_HOST_MULTIARCH = getDebHostMultiarch()
  if (DEB_HOST_MULTIARCH) {
    nPaths.push(`/usr/lib/${DEB_HOST_MULTIARCH}/nodejs/`)
  }
  return nPaths
}

export async function packageExistsAsync(pkgName: string): Promise<PortablePath | null> {
  return await getPackageJsonPathAsync(pkgName)
}

export function packageExistsSync(pkgName: string): PortablePath | null {
  return getPackageJsonPathSync(pkgName)
}

export async function getPackageJsonObjectAsync(packageJsonPath: PortablePath): Promise<any> {
  let packageJsonObject = await fs.readJsonPromise(packageJsonPath)

  return packageJsonObject
}

export function getPackageJsonObjectSync(packageJsonPath: PortablePath): any {
  let packageJsonObject = fs.readJsonSync(packageJsonPath)

  return packageJsonObject
}

export async function fetchFromDisk(locator: Locator, cwd: PortablePath): Promise<ZipFS> {
  let pkgName = locator.scope ? "@" + locator.scope + "/" + locator.name : locator.name
  let packagePath = getPackageJsonPathSync(pkgName, true)

  return await tgzUtils.makeArchiveFromDirectory(packagePath as PortablePath, {
    prefixPath: structUtils.getIdentVendorPath(locator)
  })
}

export function unAptifyPackageJsonObject(aptPackageJsonObject: any): any {
  let oldDependencies = aptPackageJsonObject.dependencies
  let oldPeerDependencies = aptPackageJsonObject.peerDependencies
  let oldDevDependencies = aptPackageJsonObject.devDependencies
  let newDependencies = {}
  let newPeerDependencies = {}
  let newDevDependencies = {}
  let packageJsonObject = { ...aptPackageJsonObject }

  if (oldDependencies) {
    for (let [dependency, range] of Object.entries<string>(oldDependencies)) {
      let newRange = range.includes(":") ? range.split(":")[1] : range
      newDependencies[dependency] = newRange
    }

    packageJsonObject.dependencies = newDependencies
  }

  if (oldPeerDependencies) {
    for (let [peerDependency, range] of Object.entries<string>(oldPeerDependencies)) {
      let newRange = range.includes(":") ? range.split(":")[1] : range
      newPeerDependencies[peerDependency] = newRange
    }

    packageJsonObject.peerDependencies = newPeerDependencies
  }

  if (oldDevDependencies) {
    for (let [devDependency, range] of Object.entries<string>(oldDevDependencies)) {
      let newRange = range.includes(":") ? range.split(":")[1] : range
      newDevDependencies[devDependency] = newRange
    }

    packageJsonObject.devDependencies = newDevDependencies
  }

  return packageJsonObject
}

export function checkDependencies(packageJsonObject: any): boolean {
  let dependencies: any | null = packageJsonObject.dependencies
  let peerDependencies: any | null = packageJsonObject.peerDependencies

  let combinedDependencies = !!dependencies
    ? !!peerDependencies
      ? [...Object.values<string>(dependencies), ...Object.values<string>(peerDependencies)]
      : [...Object.values<string>(dependencies)]
    : !!peerDependencies
      ? [...Object.values<string>(peerDependencies)]
      : []

  return !(!!(combinedDependencies.find((range) => {
    return !(!!semverUtils.validRange(range))
  })))
}

export function checkDirectory(packagePath: PortablePath): boolean {
  let content = fs.readdirSync(packagePath)

  return !(!!(content.find((fileName) => {
    let stat = fs.lstatSync(ppath.resolve(packagePath, fileName))

    return stat.isSymbolicLink()
  })))
}

function getPackageJsonPathSync(pkgName: string, dir = false): PortablePath | null {

  for (const path of NODE_PATHS) {
    const packageJsonPath = ppath.resolve(path as PortablePath, pkgName as PortablePath, (dir ? "" : "package.json") as PortablePath)
    let exists = fs.existsSync(packageJsonPath)
    if (exists) {
      return packageJsonPath
    }
  }

  return null
}

async function getPackageJsonPathAsync(pkgName: string, dir = false): Promise<PortablePath | null> {

  for (const path of NODE_PATHS) {
    const packageJsonPath = ppath.resolve(path as PortablePath, pkgName as PortablePath, (dir ? "" : "package.json") as PortablePath)
    let exists = await fs.existsPromise(packageJsonPath)
    if (exists) {
      return packageJsonPath
    }
  }

  return null
}

function getDebHostMultiarch() {
  const { stderr, stdout } = spawnSync('dpkg-architecture', ['-qDEB_HOST_MULTIARCH']);
  
  let newStdErr = stderr ?? Buffer.from("")
  let newStdOut = stdout ?? Buffer.from("")

  if(newStdErr.length > 0) {
    console.log(newStdErr.toString().trim())
  }

  return newStdOut.toString().trim()
}