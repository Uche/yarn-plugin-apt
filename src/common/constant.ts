export const STRICT_PROTOCOL = "apts:";
export const RELAXED_PROTOCOL = "aptr:";
export const NPM_PROTOCOL = "npm:";
export const NUM_NPM_INSTALL = 'numNpmInstall'
export const NUM_LOCAL_INSTALL = 'numLocalInstall'
